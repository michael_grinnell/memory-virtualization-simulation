Michael Grinnell CA1 for Operating Systems

Student Number: D00121032
Teacher: Shane Gavin

Memory Virtualization Simulation

This application simulates the structures and processes used by both an
Operating System and a Central Processing Unit to implement memory virtualization for a 16 bit virtual address space which
employs a page size of 256 bytes. 


PAGE TABLE STRUCTURE

One page table entry is 2 bytes.
The Index is the page number.
The First byte stores the physical frame.
The Second byte stores the Control bits.
For this Control bits I have only used the 4 least signifigant of these bits.

e.g 1 controlbuts byte -> 0000 1111, 0000=NOT USED, 1111- USED


Disk = 0001 If it is on Disk (1 = On Disk)
Accessed = 0010 If the entry has been Accessed (1 = has been Accessed)
Dirty = 0100 If the entry has been Modified (1 = has been Modified)
Present = 0000 If the entry contains a Mapping to a physical address (1 = mapping present, 0 = Page Fault)
