#include "lib/header/Application.h"

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	//Main Entry Point for Application
	RunSimulation();
	Start();
	return 0;
}
