#include <stdlib.h>

#include "../header/DebugHelper.h"
#include "../header/Constants.h"

// This Just Contaiins functions that are helpfull for debugging
// Should usually be romoved when Development is finished but I will leave it here to show my work
void CompareNull()
{
	// Spoiler, They Are NOT Equal!
	char null = NULL;
	char Zero = '0';

	//char Zero = 0; // This will be Equal!

	if (Zero == null)
	{
		printf("\nValues are the same!\n");
	}
	else
	{
		printf("znValues Not Equal null: %d, Zero: %d\n", null, Zero);
	}
}

void PrintArray(short* array, size_t length)
{
	//length /= sizeof(short);

	printf("Printing Array\n");

	for (size_t i = 512; i < SYS_ADDRESS_SPACE; ++i)
	{
		printf("index [%d] value:[%d]\n", i, physicalMemory[i]);
	}
}

void PrintPageTable()
{
	printf("Printing Page Table\n");

	for (size_t i = 0; i < PAGE_TABLE_SIZE; i += 2)
	{
		printf("PAGE [%d] FRAME:[%d]\n", i / 2, physicalMemory[i]);
	}
}



