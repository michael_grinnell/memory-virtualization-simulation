#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "../header/Application.h"
#include "../header/Utility.h"
#include "../header/MemoryManager.h"
#include "../header/Menu.h"

//I Know it is good practice to avoid global variables, however I felt it was ok for this purpose
// It helped as it reduced the complexity of my code and improved readability
// It is a relatively small application
// Also I was planning on changing them but I need to move onto my next assignment
unsigned char* physicalMemory = NULL;
unsigned short* TLB = NULL;

void Start()
{
	DisplayMenu();
	char input[1];
	
	printf("\nChoice: ");
	scanf("%s", input);
	
		if (!validChoice(input, 7))
		{
			printf(PRINT_RED"\nPlease Enter A valid Choice\n"PRINT_CLEAR);
			Start();
		}

		unsigned short choice = strtol(input, NULL, 10);
		unsigned short diskCount;

		switch (choice)
		{
		case 0:
			printf("SAVING DATA\n");
			WritePhysicalMemoryToFile();
			WritePageTableToFile();
			printf("GOODBYE!\n");
			exit(0);
			break;

		case 1:
			RunSimulation();
			break;

		case 2:
			PrintPageTableDescription();
			break;

		case 3:
			PrintTLB();
			break;

		case 4:
			SearchAddress();
			break;

		case 5:
			diskCount = GetDiskAddressCount();
			PrintDiskAddresses(GetAddressesOnDisk(diskCount), diskCount);
			break;

		case 6:
			WritePhysicalMemoryToFile();
			break;

		case 7:
			WritePageTableToFile();
			break;
		}

		Start();
}

void RunSimulation()
{
	PrintLine(Yellow);
	printf(PRINT_YELLOW "\n			Running Simulation\n");
	PrintLine(Yellow);

	AllocateMemory();
	InitializeMemory();
	AssignPhysicalMemory();

	WritePhysicalMemoryToFile();
	WritePageTableToFile();

	printf("\nSimulation Successfull\n");
	PrintLine(Yellow);
}

void AllocateMemory()
{
	printf("\nAllocating %d Bytes to memory\n", SYS_ADDRESS_SPACE);
	//Allocate Memory
	physicalMemory = malloc(SYS_ADDRESS_SPACE);
	TLB = malloc(TLB_SIZE * sizeof(short));
}

// Assign Random values to Physical Memory
void AssignPhysicalMemory()
{
	// Seed The Random Function
	SeedRandom();

	//Get Random value between
	unsigned short bytesToWrite = GetRandomShort(MIN_PROCESS_BYTES, MAX_PROCESS_BYTES);

	// Get random value and make sure it is a multiple of page size, so that the data starts at the beginning of a page
	unsigned short index = GetRandomMultiple(PAGE_TABLE_SIZE, (SYS_ADDRESS_SPACE - 1), FRAME_SIZE);

	printf("\nWriting %d bytes of data Starting at Location 0X%X\n", bytesToWrite, index);

	WriteToMemory(index, bytesToWrite);
}

void WriteToMemory(unsigned short index, unsigned short bytesToWrite)
{
	unsigned short bytesWritten = 0;

	// Create Array to temporarily store Disk memory, this is so the text file can be written too in one go.
	unsigned char* diskMemory = malloc(FRAME_SIZE * 2);
	unsigned char* writeLocation;

	bool memoryFull;

	
	while (bytesWritten < bytesToWrite)
	{
		// Simulates running out of memory for last two Frames
		memoryFull = bytesWritten >= bytesToWrite - (FRAME_SIZE * 2);

		//if the memory is full the index should start at 0 again
		index = memoryFull ? (FRAME_SIZE * 2) - (bytesToWrite - bytesWritten) : index;
		
		// If Memory is full tell the write location to point to the disk memory array
		writeLocation = memoryFull ? &diskMemory[index] : &physicalMemory[index];
		*writeLocation = GetRandomChar();

		//If the data moves to another page write Entry the page table
		if (!(index % FRAME_SIZE))
			WriteToPageTable(index, (memoryFull ? Present | Disk : Present));

		//if The data reaches the end of the physical memory, Wrap around and find the next available free block
		if (++index >= SYS_ADDRESS_SPACE - 1)
			index = PAGE_TABLE_SIZE;//TODO GET Next Address Not In Use

		++bytesWritten;
	}
	printf("\nMemory Full At %d Bytes, Writing Remaining Data To Swap File \n", bytesWritten - (FRAME_SIZE * 2));
	//Write Disk Memory To File
	WriteToSwapFile(diskMemory);
}

void SearchAddress()
{
	char input[6];
	unsigned int address;
	PrintLine(Green);
	printf("\nSEARCHING VIRTUAL ADDRESS - Type \"Exit\" -> Main Menu\n");
	PrintLine(Green);
	printf("\nVirtual Address: ");
	scanf("%s", input);

	if (strcasecmp(input, "Exit") == 0)
		Start();

	if (validHex(input))
		address = strtol(input, NULL, 16);
	else
	{
		printf(PRINT_RED"\nPlease Enter A valid 16 bit HEX Address\n"PRINT_CLEAR);
		SearchAddress();
	}

	unsigned short physicalAddress = TranslateAddress(address);

	if (physicalAddress == '\0')
	{
		printf(PRINT_RED"\nENTRY Contains NO Mapping\n"PRINT_CLEAR);
		SearchAddress();
	}
	
	printf(PRINT_GREEN"\nVIRTUAL ADDRESS: 0X%X\n", address);
	printf("\nMAPS TO PHYSICAL ADDRESS: 0X%X\n" , physicalAddress);
	printf("\nCONTENT: %c\n"PRINT_CLEAR, physicalMemory[physicalAddress]);

	printf(PRINT_YELLOW"\nNOTE: txt files will not be updated until you exit the program from the main menu");
	printf("\nRe-Run the Simulation OR save to file from main menu"PRINT_CLEAR);
	PrintLine(Green);
	SearchAddress();
}
