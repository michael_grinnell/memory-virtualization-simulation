#include "../header/TLB.h"
#include "../header/MemoryManager.h"
#include "../header/Utility.h"
#include <stdio.h>

// My Implimentation Of the TLB is not very efficiant but it functions in a similar way as a real TLB would
// To keep track of the LRU i just sort the array every time the TLB is Modified
// I keep the LRU in position 0 and the last entry at the top

unsigned short TLBLookup(unsigned char page)
{
	unsigned char found = 0;
	printf("\nChecking For Page: 0X%X In TLB \n", page);
	// Loop through TLB 
	for (unsigned short i = 0; i < TLB_SIZE; ++i)
	{
		// If the first 8 bits are the same as the page and the last 8 bits are not null we found a match
		if ((TLB[i] >> BYTE) == page && !(TLB[i] & 0XFF) == '\0')
		{
			printf(PRINT_GREEN"\nMapping Found IN TLB \n"PRINT_CLEAR);
			// Move the entry to the top, so the last accesed will always be at the top, the LRU will always be at the bottom
			MoveEntryToTop(i);
			found = TLB_SIZE;
			break;
		}
	}

	if (!found)
	{
		printf(PRINT_RED"\nNO Entry Found IN TLB\n"PRINT_CLEAR);
		printf("\nChecking Page Table Entry Control bits to see if entry is Mapped\n");
	}
		
	// Found will be 0 if No entrys were found
	return found;
}

void SwapEntry(unsigned short *a, unsigned short  *b)
{
	unsigned short temp = *a;
	*a = *b;
	*b = temp;
}

void MoveEntryToTop(unsigned short pos)
{
	printf("\nMoveing TLB Entry to Most Recently Used\n");
	// Similar to Bubble sort, This will take an entry and move it to the last pos in the array while maintaining order
	for (unsigned short i = pos; i < (TLB_SIZE -1); ++i)
	{
		SwapEntry(&TLB[i], &TLB[i + 1]);
	}
}

void InsertEntry(unsigned char page, unsigned char frame)
{
	printf("\nWriting to TLB, PAGE: 0X%X, FRAME: 0X%X \n", page, frame);
	unsigned short entry = (page << BYTE) | frame;

	// Move all entrys down one position, terribly inefficient
	for (int i = 0; i < (TLB_SIZE) - 1; ++i)
		TLB[i] = TLB[i + 1];

	// Set the last Pos to the new entry
	TLB[TLB_SIZE -1 ] = entry;
}
