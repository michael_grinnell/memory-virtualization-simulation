#include <stdio.h>
#include <stdlib.h>
#include "../header/MemoryManager.h"
#include "../header/Utility.h"
#include "../header/Menu.h"
#include "../header/TLB.h"

void WriteToPageTable(unsigned short physicalIndex, ControlBits controlBits)
{
	//Get Free index From Page Table
	unsigned short freeIndex = GetNextFreePage();

	// Set first 8 bits to the index of the physical frame
	physicalMemory[freeIndex] = physicalIndex / FRAME_SIZE;

	// Set the next 8 bits to the control bits
	physicalMemory[freeIndex + 1] = controlBits; //Sets the control bits
}

void ModifyPageTableEntry(unsigned char page, unsigned short frame, ControlBits controlBits)
{
	printf("\nUpdating Page Table Entry To Point to new Location in physical Memory\n");
	// Set first 8 bits to the index of the physical frame
	physicalMemory[page] = frame;
	// Set the next 8 bits to the control bits
	physicalMemory[page + 1] = controlBits; //Sets the control bits
}

unsigned char GetNextFreePage()
{
	unsigned char freeIndex = 0;

	//Loop Through PageTable
	for (unsigned short i = 0; i < PAGE_TABLE_SIZE; i+= 2)
	{
		if (!MemoryInUse(i))
		{
			//if the memory is not in use return the free index;
			freeIndex = i;
			break;
		}
	}
	
	return freeIndex;
}

unsigned short GetNextFreeFrame()
{
	printf("\nFinding Next Free Frame in Physical Memory\n");
	unsigned short freeFrame = PAGE_TABLE_SIZE;

	//Loop Through Physical Memory
	for (unsigned short i = PAGE_TABLE_SIZE; i < SYS_ADDRESS_SPACE; ++i)
	{
		if (physicalMemory[i] == '\0')
		{
			//if the memory is not in use return the free index;
			freeFrame = RoundUp(i, FRAME_SIZE);
			break;
		}
	}

	return freeFrame;
}

unsigned short GetDiskAddressCount()
{
	unsigned short count = 0;

	for (size_t i = 0; i < PAGE_TABLE_SIZE; i += 2)
		if ((physicalMemory[i + 1] & Disk))
			count++;

	return count;
}

unsigned short* GetAddressesOnDisk(unsigned short count)
{
	unsigned short index = 0;
	unsigned short* diskAddresses = malloc(sizeof(short) * count);

	for (size_t i = 0; i < PAGE_TABLE_SIZE; i+=2)
	{
		if ((physicalMemory[i + 1] & Disk))
		{
			diskAddresses[index] = i / 2;
			index++;
		}
	}

	return diskAddresses;
}

bool MemoryInUse(unsigned short index)
{
	//Check Bits to see if its in use
	return physicalMemory[index + 1] & Present;
}

void InitializeMemory()
{
	// Initialize Entries to Null
	// This is how we know that the address is not in use, We also need the control bits to be initialized to 0000
	// Null is not the same as char '0', run the below function located in DebugHelper.c
	// void CompareNull()

	for (size_t i = 0; i < 8; i++)
		TLB[i] = '\0';

	for (size_t i = 0; i < SYS_ADDRESS_SPACE; ++i)
		physicalMemory[i] = '\0';
}

unsigned short TranslateAddress(unsigned short virtualAddress)
{
	// Isolate Offset Bits
	printf("\nSearching Virtual Address: 0X%X : ", virtualAddress);
	PrintBinary(virtualAddress);

	printf("\nRemove Offset Bits By Applying Mask\n");
	unsigned short pageOffset = GetOffsetBits(virtualAddress);

	printf("\nOFFSET: 0X%X : ", pageOffset);
	PrintBinary(pageOffset);

	printf("\nGet PAGE by bitshifting to the right by 8 bits\n");
	unsigned char page = virtualAddress >> BYTE;

	printf("\nPAGE: 0X%X : ", page);
	PrintBinary(page);

	if (!TLBLookup(page))
	{
		if (!(physicalMemory[(page * 2) + 1] & Present))//Check if the entry is Mapped
			return 0;
		else
			InsertEntry(page, PageTableLookup(page));
	}

	printf("\nAppending Offset to physical Frame\n");	

	//Returns the last Entry in the TLB & with 0X00FF to get the Physical Frame, Multiply by frame size and add the offset
	return ((TLB[TLB_SIZE - 1] & 0X00FF) * FRAME_SIZE) + pageOffset;
}

unsigned short PageTableLookup(unsigned char page)
{
	printf("\nChecking Page Table Entry Control bits to see if entry is On Disk\n");
	// Checkif Control bits say Entry is on disk, If so Do Swapping
	if (physicalMemory[(page * 2) + 1] & Disk)
		SwapMemoryFromDisk(page);

	return physicalMemory[page * 2];
}

void SwapMemoryFromDisk(unsigned char page)
{
	printf(PRINT_RED"\nMemory IS Located On Disk, Swapping Data\n"PRINT_CLEAR);
	char* memory = ReadSwapFile(physicalMemory[page *2]);

	unsigned short freeFrame = GetNextFreeFrame();

	ModifyPageTableEntry((page * 2), freeFrame / FRAME_SIZE, (Accessed | Dirty | Present));

	printf("\nWriting Data To Physical Memory\n");
	for (size_t i = 0; i < FRAME_SIZE; i++)
		physicalMemory[freeFrame + i] = memory[i];
}
