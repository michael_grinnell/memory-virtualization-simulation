#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#include <string.h>

#include "../header/Utility.h"

void SeedRandom()
{
	//Seed the Random Function
	srand(time(NULL));

	for (size_t i = 0; i < 100; ++i)
		rand();
}

unsigned short GetRandomShort(unsigned short min, unsigned short max)
{
	return (rand() % (max - min)) + min;
}

unsigned short GetRandomMultiple(unsigned short min, unsigned short max, unsigned short multiple)
{
	return RoundUp(GetRandomShort(min, max), multiple);
}

unsigned char GetRandomChar()
{
	return (rand() % (ASCII_MAX - ASCII_MIN)) + ASCII_MIN;
}

unsigned short RoundUp(unsigned short number, unsigned short multiple)
{
	if (!(number % multiple))
		return number;

	return (number - (number % multiple)) + multiple;
}

void WritePhysicalMemoryToFile()
{
	printf("\nWRITING PHYSICAL MEMORY TO FILE\n");
	FILE* file = fopen(PHYSICAL_MEMORY_FILE_PATH, "w");

	//TODO Supply Legend
	fprintf(file, "| %-10s | %-10s | %-10s | %-10s | %-10s |\n", "Address", "Frame", "Content", "Value", "In Use");
	fprintf(file, "| __________ | __________ | __________ | __________ | __________ |\n");

	unsigned short frame = 1;
	// Starts at Frame 2 as first two frames are occupied by page table
	for (unsigned int i = PAGE_TABLE_SIZE; i < SYS_ADDRESS_SPACE; ++i)
	{
		if (i % FRAME_SIZE == 0)
			++frame;

		fprintf(file, "| 0X%-8X | 0X%-8X | 0X%-8X | %-10c | %-10d |\n", i, frame, physicalMemory[i], physicalMemory[i], physicalMemory[i] != '\0');
	}

	//Note if you are viewing this in Notepad++, it prints 3 characters for NULL values, which is really annoying as it offsets my neat table
	fclose(file);
	printf(PRINT_GREEN"\nFile Saved!\n"PRINT_CLEAR);
}

void WritePageTableToFile()
{
	printf("\nWRITING PAGE TABLE TO FILE\n");
	FILE* file = fopen(PAGE_TABLE_FILE_PATH, "w");

	//TODO Supply Legend
	fprintf(file, "| %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |\n", "Page", "Frame", "Present", "Dirty", "Accessed", "Disk");
	fprintf(file, "| __________ | __________ | __________ | __________ | __________ | __________ |\n");

	unsigned char controlBits;

	unsigned char present;
	unsigned char dirty;
	unsigned char accessed;
	unsigned char disk;

	// Starts at Frame 2 as first two frames are occupied by page table
	for (unsigned int i = 0; i < PAGE_TABLE_SIZE; i += 2)
	{
		controlBits = physicalMemory[i + 1];

		present = (Present & controlBits) != false;
		dirty = (Dirty & controlBits) != false;
		accessed = (Accessed & controlBits) != false;
		disk = (Disk & controlBits) != false;

		fprintf(file, "| 0X%-8X | 0X%-8X | %-10d | %-10d | %-10d | %-10d |\n", i/2, physicalMemory[i], present, dirty, accessed, disk);
	}

	//Note if you are viewing this in Notepad++, it prints 3 characters for NUL values, which is really annoying as it offsets my neat table
	fclose(file);

	printf(PRINT_GREEN"\nFile Saved!\n"PRINT_CLEAR);
}

void WriteToSwapFile(unsigned char* memory)
{
	FILE* file = fopen(SWAP_FILE_PATH, "w");

	for (size_t i = 0; i < PAGE_TABLE_SIZE; ++i)
		fprintf(file, "%c", memory[i]);

	fclose(file);
}

char* ReadSwapFile(unsigned char page)
{
	printf("\nReading Data From Swap File\n");
	char* buffer = malloc((FRAME_SIZE + 1));

	FILE* file = fopen(SWAP_FILE_PATH, "r");

	fseek(file, page * FRAME_SIZE, SEEK_SET);
	fread(buffer, FRAME_SIZE, 1, file);

	fclose(file);

	return buffer;
}

// Created by Shane Gavin <shane.gavin@dkit.ie> for Y4 projects
// in 2018/19 Operating Systems Module. 
unsigned short CreateMask(unsigned char bits, MaskType type)
{
	unsigned short mask = 0;

	//Sets Masker = to type, if the type is Low Order Mask then set it to BITMASK_LSB, otherwise set it to BITMASK_MSB
	unsigned short masker = type == LowOrderMask ? BITMASK_LSB : BITMASK_MSB;

	for (unsigned char i = 0; i < bits; ++i) {

		mask |= masker;

		if (type == LowOrderMask) {
			masker <<= BIT;
		}
		else {
			masker >>= BIT;
		}
	}

	return mask;
}

unsigned short GetOffsetBits(unsigned short virtualAddress)
{
	//Get mask for first 8 bits then & with virtual address
	return CreateMask(BYTE, LowOrderMask) & virtualAddress;
}

bool validChoice(char* input, unsigned short max)
{

	if (input[strspn(input, "0123456789")] == 0)
	{			
		if (strtol(input, NULL, 10) <= max)
			return true;
	}

	return false;
}

bool validHex(char* input)
{
	if (input[strspn(input, "0Xx123456789abcdefABCDEF")] == 0)
	{
		if (strtol(input, NULL, 16) == 0X2A)//42
			printf(PRINT_RED"\n\nDON'T PANIC!\n\n"PRINT_CLEAR);
		if (strtol(input, NULL, 16) <= 0XFFFF)
			return true;
	}

	return false;
}
