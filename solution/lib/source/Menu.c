#include<stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../header/Menu.h"
#include "../header/Constants.h"

void DisplayMenu()
{
	PrintLine(Green);
	printf(PRINT_GREEN "			MICHAEL GRINNELL\n");
	printf("			CA1 OPERATING SYSTEMS\n");
	printf("			MEMORY VIRTUALIZATION SIMULATION");
	PrintLine(Green);
	printf("\n(1) RUN SIMULATIION\n");
	printf("(2) DISPLAY PAGE TABLE STRUCTURE\n");
	printf("(3) VIEW TLB ENTRIES\n");
	printf("(4) ENTER VIRTUAL ADDRESS\n");
	printf("(5) DISPLAY ADDRESSES ON DISK\n");
	printf("(6) WRITE PHYSICAL MEMORY TO FILE\n");
	printf("(7) WRITE PAGE TABLE TO FILE\n");
	printf("(0) EXIT\n");

	
}

void PrintLine(Colour colour)
{
	switch (colour)
	{
	case Red:
		printf(PRINT_RED);
		break;
	case Green:
		printf(PRINT_GREEN);
		break;
	case Blue:
		printf(PRINT_BLUE);
		break;
	case Yellow:
		printf(PRINT_YELLOW);
		break;
	}


	printf("\n");

	for (int i = 0; i < MENU_WIDTH; ++i)
		printf("_");

	printf(PRINT_CLEAR "\n");
}

void PrintDiskAddresses(unsigned short* addresses, unsigned short length)
{
	if (length > 0)
		printf("\nThe Virtual Addresses Stored On Disk Are\n");
	else
		printf(PRINT_RED"\nNO ENTRIES ON DISK\n"PRINT_CLEAR);

	for (size_t i = 0; i < length; ++i)
		printf("\n0X%X00\n", addresses[i]);
}

void PrintPageTableDescription()
{
	PrintLine(Green);
	printf(PRINT_GREEN "\n			PAGE TABLE ENTRY STRUCTURE\n");
	PrintLine(Green);

	printf("\nOne Page Table Entry is Two Bytes\n");
	printf("\nBYTE 1 -> Physical Page Frame\n");
	printf("\nBYTE 2 -> Control Bits\n");
	
	PrintLine(Yellow);
	printf("\n|  [Index(Page Number)]  | [Physical Frame] | [Control Bits]   |");
	PrintLine(Yellow);
	printf("\n CONTROL BITS: [0000 1111] = [0000]-> NOT USED [1111]-> USED \n");

	printf("\n [1000->PRESENT] | [0100->DIRTY] | [0010->ACCESSED] | [0001->DISK] \n");

	printf("\nPRESENT = If the entry contains a Mapping to a physical address (1 = mapping present, 0 = Page Fault)");
	printf("\nDIRTY = If the entry has been Modified (1 = has been Modified)");
	printf("\nACCESSED = If the entry has been Accessed (1 = has been Accessed)");
	printf("\nDISK = If the entry is stored on Disk (1 = On Disk)");

	PrintLine(Green);
}

void PrintTLB()
{
	PrintLine(Green);
	printf("\n			Displaying TLB Entries!\n");
	PrintLine(Green);
	printf("\n");

	for (unsigned int i = 0; i < 8; i++)
		printf("INDEX: [%d] VIRTUAL:[0X%X] Physical:[0X%X]\n", i, TLB[i] >> BYTE, TLB[i] & 0XFF);

	printf(PRINT_YELLOW"\nThe TLB will be empty at the start of each simulation\nAs virtual addresses are looked up it will be updated\n"PRINT_CLEAR);
	PrintLine(Green);
}

void PrintBinary(unsigned short num)
{
	unsigned short split = 4;

	for (int i = SHORT_BITS - 1; i >= 0; --i)
	{
		if (num >> i & BIT)
			printf("1");
		else
			printf("0");

		if (!(i % split))
			printf(" ");
	}

	printf("\n");
}
