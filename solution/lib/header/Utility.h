#ifndef UTILITY_H
#define UTILITY_H
#include "Constants.h"

void SeedRandom();
unsigned short GetRandomShort(unsigned short min, unsigned short max);
unsigned short GetRandomMultiple(unsigned short min, unsigned short max, unsigned short multiple);
unsigned char GetRandomChar();

unsigned short RoundUp(unsigned short number, unsigned short multiple);

void WritePhysicalMemoryToFile();
void WritePageTableToFile();
void WriteToSwapFile(unsigned char* memory);
char* ReadSwapFile(unsigned char page);

bool validHex(char* input);
bool validChoice(char* input, unsigned short max);

unsigned short CreateMask(unsigned char bits, MaskType type);
unsigned short GetOffsetBits(unsigned short virtualAddress);

#endif // UTILITY_H
