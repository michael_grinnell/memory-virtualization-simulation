#ifndef DEBUG_HELPER_H
#define DEBUG_HELPER_H

void CompareNull();
void PrintArray(short* array, size_t length);
void PrintPageTable();

#endif // !DEBUG_HELPER_H
