#ifndef APPLICATION_H
#define APPLICATION_H

#include "Constants.h"

// FUNCTION DECLORATIONS
void Start();
void RunSimulation();
void AllocateMemory();
void AssignPhysicalMemory();
void WriteToMemory(unsigned short index, unsigned short bytesToWrite);
void SearchAddress();

#endif // APPLICATION_H
