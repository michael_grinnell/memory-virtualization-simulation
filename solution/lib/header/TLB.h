#ifndef TLB_H
#define TLB_H

#include "Constants.h"

unsigned short TLBLookup(unsigned char page);
void SwapEntry(unsigned short *a, unsigned short  *b);
void MoveEntryToTop(unsigned short pos);
void InsertEntry(unsigned char page, unsigned char frame);

#endif // !TLB_H
