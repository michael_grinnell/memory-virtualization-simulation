#ifndef MENU_H
#define MENU_H

#include"Constants.h"

void DisplayMenu();
void PrintLine(Colour colour);
void PrintDiskAddresses(unsigned short* addresses, unsigned short length);
void PrintPageTableDescription();
void PrintTLB();
void PrintBinary(unsigned short num);

#endif // !MENU_H
