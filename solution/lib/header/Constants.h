#ifndef CONSTANTS_H
#define CONSTANTS_H

// ENUMS
typedef enum {
	LowOrderMask,
	HighOrderMask
} MaskType;

typedef enum {
	false,
	true
} bool;

typedef enum {
	None = 0,				// 0000
	Disk = 1 << 0,			// 0001 If it is on Disk (1 = On Disk)
	Accessed = 1 << 1,		// 0010 If the entry has been Accessed (1 = has been Accessed)
	Dirty = 1 << 2,			// 0100 If the entry has been Modified (1 = has been Modified)
	Present = 1 << 3		// 1000 If the entry contains a Mapping to a physical address (1 = mapping present, 0 = Page Fault)
} ControlBits;

typedef enum {
	Red,
	Green,
	Blue,
	Yellow
} Colour;

//Globals - TODO REMOVE GLOBALS
extern unsigned char* physicalMemory;
extern unsigned short* TLB;

//CONSTANTS
// Values are in bytes unless otherwise specified
#define BIT 1
#define BYTE 8
#define SHORT_BITS 16

#define ASCII_MIN 32
#define ASCII_MAX 127

#define BITMASK_LSB 0x1
#define BITMASK_MSB 0x80
#define BITMASK_FULL 0xFF

#define VIRTUAL_ADDRESS_SPACE 2  
#define SYS_BITS 16
#define TLB_SIZE 8 // 8 entries
#define FRAME_SIZE 256
#define PAGE_TABLE_SIZE 512
#define SYS_ADDRESS_SPACE 65536 // 2 ^ 16

#define MIN_PROCESS_BYTES 2048
#define MAX_PROCESS_BYTES 20480

#define PHYSICAL_MEMORY_FILE_PATH "data/physical_memory.txt"
#define PAGE_TABLE_FILE_PATH "data/page_table.txt"
#define SWAP_FILE_PATH  "data/swap_file.txt"

#define PRINT_RED "\x1b[31m"
#define PRINT_GREEN "\x1b[32m"
#define PRINT_BLUE "\x1b[34m"
#define PRINT_YELLOW "\x1b[33m"
#define PRINT_CLEAR "\x1b[0m"

#define MENU_WIDTH 100

#endif // !CONSTANTS_H
