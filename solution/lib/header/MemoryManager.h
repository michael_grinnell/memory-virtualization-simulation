#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include "Constants.h"

void WriteToPageTable(unsigned short physicalIndex, ControlBits controlBits);
void ModifyPageTableEntry(unsigned char page, unsigned short frame, ControlBits controlBits);
unsigned char GetNextFreePage();
unsigned short GetNextFreeFrame();
bool MemoryInUse(unsigned short index);
void InitializeMemory();
unsigned short TranslateAddress(unsigned short virtualAddress);
unsigned short PageTableLookup(unsigned char page);
unsigned short GetDiskAddressCount();
unsigned short* GetAddressesOnDisk(unsigned short count);
void SwapMemoryFromDisk(unsigned char page);

#endif //MEMORY_MANAGER_H
